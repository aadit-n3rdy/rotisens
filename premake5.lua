workspace "rotisens"
	configurations { "debug", "release" }
	buildoptions { "--std=c99", "-Wall" }
	toolset "gcc"
	filter "configurations:debug"
		defines { "DEBUG" }
		symbols "On"
	filter "configurations:release"
		defines { "NDEBUG" }
		optimize "On"


include "extern/glad/glad.lua"
include	"extern/glfw/glfw.lua"
include	"rotisens/rotisens.lua"
include	"test/test.lua"
