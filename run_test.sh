#! /bin/zsh

if test -z $1
then
	./test/platformer/bin/debug/platformer
else
	./test/$1/bin/debug/$1
fi
