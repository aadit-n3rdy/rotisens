#version 330 core

layout (location = 0) in vec2 vert_pos;
layout (location = 1) in vec2 vert_texcoord;

uniform mat3 mvp;
uniform mat3 tex_mat;
uniform float z;
out vec2 frag_texcoord;

void main() {
	vec3 pos = mvp*vec3(vert_pos, 1.0f);
	gl_Position = vec4(pos.xy, -z, 1.0f);
	frag_texcoord = (tex_mat * vec3(vert_texcoord, 1.0f)).xy;
}
