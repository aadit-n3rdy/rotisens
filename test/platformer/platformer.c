#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glad/glad.h>

#include "stb_image.h"

#include "matrix.h"
#include "shader.h"
#include "vec.h"
#include "color.h"
#include "texture.h"
#include "window.h"
#include "logger.h"
#include "sprite.h"
#include "transform.h"
#include "renderer.h"
#include "animator.h"

#define ROTI_ALIGN_CENTER 0
#define ROTI_ALIGN_TOPLEFT 4
#define ROTI_ALIGN_TOPRIGHT 8
#define ROTI_ALIGN_BOTTOMLEFT 12
#define ROTI_ALIGN_BOTTOMRIGHT 16

void gl_message_callback ( GLenum source,
		GLenum type,
		GLuint id,
		GLenum severity,
		GLsizei length,
		const GLchar* message,
		const void* userParam )
{
	fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
			( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
			type, severity, message );
}

#define WIDTH 1280
#define HEIGHT 720

int main() {

	roti_init();

	roti_set_color_log(true);

	roti_window window;
	window.mode = ROTI_WIN_WINDOWED;
	window.resizeable = false;
	window.width = 1280;
	window.height = 720;
	window.monitor = NULL;
	window.title = "ROTISENS";
	window.vsync = true;

	roti_init_window(&window);

	roti_transform camera;
	camera.pos = (vec2){100, 100};
	camera.angle = 0;

	roti_renderer renderer;
	roti_renderer_init(&renderer, ROTI_DEFAULT_VERT_PATH, ROTI_DEFAULT_FRAG_PATH, &window, &camera);

	roti_animator anim;
	roti_animator_init(&anim);

	srand(time(NULL));

	roti_transform transform;
	transform.pos = (vec2){640, 360};
	transform.angle = 0.0f;

	roti_transform bg_transform;
	bg_transform.pos = (vec2){0, 0};
	bg_transform.angle = 0.0f;

	roti_tex tex;
	tex.channels = 4;
	roti_gen_tex(&tex, "test/platformer/assets/Knight/noBKG_KnightRun_strip.png", 1);

	roti_tex bg_tex;
	bg_tex.channels = 3;
	roti_gen_tex(&bg_tex, "test/platformer/assets/bg.png", 1);

	roti_sprite sprite;
	sprite.tex_topleft = (vec2){0, 0};
	sprite.tex_size = (vec2){96, 64};
	sprite.alignment = ROTI_ALIGN_CENTER;
	sprite.offset = (vec2){0, 0};
	sprite.size = (vec2){192, 128};
	sprite.transform = &transform;
	sprite.tex = &tex;
	sprite.col = (color){1.0, 1.0, 1.0, 1};
	sprite.z = 1;

	roti_animated knight_animated;
	roti_animated_init(&knight_animated, &sprite);

	roti_animation run;
	run.delta = 1.0f/24.0f;
	run.total_frames = 8;
	run.start = (vec2){0, 0};
	run.offset = (vec2){96, 64};

	roti_animated_set_animation(&knight_animated, &run);

	roti_sprite bg_sprite;
	bg_sprite.tex_topleft = (vec2){0, 0};
	bg_sprite.tex_size = (vec2){2560, 1440};
	bg_sprite.alignment = ROTI_ALIGN_TOPLEFT;
	bg_sprite.offset = (vec2){0, 0};
	bg_sprite.size = (vec2){1280, 720};
	bg_sprite.transform = &bg_transform;
	bg_sprite.tex = &bg_tex;
	bg_sprite.col = (color){1.0, 1.0, 1.0, 1.0};
	bg_sprite.z = 0;

	roti_renderer_register_sprite(&renderer, &bg_sprite);
	roti_renderer_register_sprite(&renderer, &sprite);

	roti_register_animated(&anim, &knight_animated);

	color bg = (color){0.5, 0.5, 0.6, 0.1};

	roti_set_gl_debug(true);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_DEPTH_TEST);

	roti_set_bg(bg);

	while (!glfwWindowShouldClose(window.ptr)) {

		roti_clear();

		if (glfwGetKey(window.ptr, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
			glfwSetWindowShouldClose(window.ptr, GLFW_TRUE);
		}

		roti_animator_update(&anim);

		roti_render_sprites(&renderer);
		
		glfwSwapBuffers(window.ptr);
		glfwPollEvents();
	}

	glfwTerminate();

	return 0;
}
