#version 330 core

uniform vec4 color_mask;
uniform sampler2D tex;

in vec2 frag_texcoord;

void main() {
	vec4 col = color_mask * texture(tex, frag_texcoord);
	gl_FragColor = col;
};
