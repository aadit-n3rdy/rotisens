workspace "glad"
	configurations { "debug", "release" }
	filter "configurations:debug"
		defines { "DEBUG" }
		symbols "On"
	filter "configurations:release"
		defines { "NDEBUG" }
		optimize "On"

include "glad.lua"
