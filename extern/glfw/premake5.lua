workspace "glfw"
	configurations { "debug", "release" }
	filter "configurations:debug"
		defines { "DEBUG" }
		symbols "On"
	filter "configurations:release"
		defines { "NDEBUG" }
		optimize "On"

include "glfw.lua"

