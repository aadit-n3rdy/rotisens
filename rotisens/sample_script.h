// Include script header in reg_scripts.c (in project src),
// reg_scripts.c includes reg_scripts.h for roti_register_scripts(void *scene)
//
// Compile script .c with game
//
// for each script:
// create template (contains ptrs to funcs)
// add template to component tree
//
// struct roti_script_template {
// 	uint32_t script_id;
// 	char *script_name;
// 	void (*update)(void *data, uint32_t entt_id, void *scene);
// 	<other funcs>
// };
//
// one tree for components, each component is a tree of all instances of that component
//
// one tree for scripts (can have multip instances of same script template)

#include <script1.h>
#include <script2.h>

void reg_scripts() {
	scripts_template* script1 = malloc;
	script1->update = script1_update; //function
	script1->name = script1_name;
	script1->hash = hash(script1_name);
	component_tree = maram_insert(component_tree, script1);

	scripts_template* script2 = malloc;
	script2->update = NULL;
	script2->name = script2_name;
	script2->hash = hash(script2_name);
	component_tree = maram_insert(component_tree, script2);
}

//Sample script1.h:

struct script1_data {
	maram_header header;
	int bulletspeed;
	position* positioncomp;
};

void script1_init(script1_data* data, int entt, scene);
void script1_update();

//End of sample script1.h
//
//
//void add_component(script_template* tmp, void* data, int entt) {
//	maram_init_header(data, entt);
//	tmp->init(data, entt, scene bleh bleh);
//	tmp->tree = maram_insert(tmp->tree, data);
//}
//
//sample script1.c:

void script1_init(script1_data* data, int entt, scene) {
	log(starting);
	data->positioncomp = scene->getcomponent("position", entt);
}

void script1_update() {
	scene.log("hehe");
	positioncomp.x += bulletspeed;
}
