#ifndef ROTI_MATRIX_H
#define ROTI_MATRIX_H

#include "vec.h"

typedef float mat3[3][3];

int roti_multip_mat3(mat3 m1, mat3 m2, mat3 dest);

vec3 roti_multip_mat3_vec3(mat3 m, vec3 v);

int roti_translate_mat3(vec2 v, mat3 dest);

int roti_scale_mat3(vec2 v, mat3 dest);

int roti_rot_mat3(float theta, mat3 dest);

int roti_print_mat3(mat3 m);


#endif

