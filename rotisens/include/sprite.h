#ifndef ROTI_SPRITE_H
#define ROTI_SPRITE_H

#include "vec.h"
#include "texture.h"
#include "matrix.h"
#include "transform.h"
#include "color.h"

#define ROTI_ALIGN_CENTER 0
#define ROTI_ALIGN_TOPLEFT 4
#define ROTI_ALIGN_TOPRIGHT 8
#define ROTI_ALIGN_BOTTOMLEFT 12
#define ROTI_ALIGN_BOTTOMRIGHT 16


typedef struct roti_sprite {
	vec2 offset;
	vec2 size;
	unsigned char alignment;
	vec2 tex_topleft;
	vec2 tex_size;
	struct roti_tex *tex;
	struct roti_transform *transform;
	mat3 texmat;
	mat3 transmat;
	color col;
	float z;
} roti_sprite;

int roti_sprite_gen_texmat(struct roti_sprite* sprite);

int roti_sprite_gen_mvp(struct roti_sprite* sprite);

int roti_sprite_gen_mat(struct roti_sprite* sprite);

#endif
