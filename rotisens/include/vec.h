#ifndef ROTI_VEC_H
#define ROTI_VEC_H

typedef struct vec2 {
	float x;
	float y;
} __attribute__((packed)) vec2;

typedef struct vec3 {
	float x;
	float y;
	float z;
} __attribute__((packed)) vec3;

vec3 roti_cross_vec3(vec3 v1, vec3 v2);

float roti_dot_vec3(vec3 v1, vec3 v2);

vec3 roti_normalise_vec3(vec3 v);

vec3 roti_multip_vec3_float(vec3 v, float f);

vec3 roti_add_vec3(vec3 v1, vec3 v2);

vec2 roti_normalise_vec2(vec2);

vec2 roti_multip_vec2_float(vec2 v, float f);

vec2 roti_add_vec2(vec2 v1, vec2 v2);

vec2 roti_rotate_vec2(vec2 v, float angle);

#endif
