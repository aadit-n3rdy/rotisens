#ifndef ROTI_RENDERER_H
#define ROTI_RENDERER_H

#include "sprite.h"
#include "shader.h"
#include "window.h"
#include "transform.h"

#define ROTI_DEFAULT_VERT_PATH "vert.glsl"
#define ROTI_DEFAULT_FRAG_PATH "frag.glsl"

typedef struct roti_renderer {
	roti_sprite *sprites[512];
	roti_transform *camera;
	unsigned int vao;
	unsigned int vbo;
	roti_shader shader;
	roti_window *window;
	mat3 mvp;
	mat3 cam_mat;
} roti_renderer;

int roti_renderer_register_sprite(roti_renderer *renderer, roti_sprite *sprite);

int roti_renderer_load_shaders(roti_renderer *renderer, char* vert_path, char* frag_path);

int roti_render_sprites(roti_renderer *renderer);

int roti_renderer_init(roti_renderer *renderer, 
		char* vert_path, char* frag_path, 
		roti_window *window, roti_transform *camera);

#endif
