#ifndef ROTI_WINDOW_H
#define ROTI_WINDOW_H

#include <stdbool.h>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include "matrix.h"
#include "color.h"

#define ROTI_WIN_FULLSCRN 0
#define ROTI_WIN_WINDOWED_FULLSCRN 1
#define ROTI_WIN_WINDOWED 2


typedef GLFWmonitor* roti_monitor;

typedef struct roti_window {
	GLFWwindow *ptr;
	roti_monitor monitor;
	char *title;
	int width;
	int height;
	bool resizeable;
	unsigned char mode;
	bool vsync;
	mat3 mat;
} roti_window;

int roti_init();

int roti_init_window(roti_window *win);

int roti_gen_winmat(roti_window *win);

int roti_set_bg(color col);

int roti_clear();
#endif
