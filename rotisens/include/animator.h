#ifndef ROTI_ANIMATOR_H
#define ROTI_ANIMATOR_H

#include "sprite.h"


typedef struct roti_animation {
	double delta;
	unsigned int total_frames;
	vec2 start;
	vec2 offset;
} roti_animation;

typedef struct roti_animated {
	roti_sprite *sprite;
	roti_animation *cur_animation;
	uint32_t cur_frame;
	double last_update;
} roti_animated;

typedef struct roti_animator {
	roti_animated *animations[512];
} roti_animator;

int roti_animated_init(roti_animated *animated, roti_sprite *sprite);

int roti_animated_set_animation(roti_animated *animated, roti_animation *animation);

int roti_animator_init(roti_animator *animator);

int roti_register_animated(roti_animator *animator, roti_animated *animated);

int roti_animator_update(roti_animator *animate);

#endif
