#ifndef ROTI_TEXTURE_H
#define ROTI_TEXTURE_H

#include <stdint.h>
#include <stdbool.h>

#include "matrix.h"

#define ROTI_TEX_FILTER_LINEAR 0
#define ROTI_TEX_FILTER_NEAREST 1

typedef struct roti_tex {
	uint32_t id;
	int width;
	int height;
	int channels;
	mat3 mat;
} roti_tex;

int roti_gen_tex(roti_tex *tex, const char* path, unsigned char pixel_art);
int roti_gen_tex_raw(roti_tex *tex, const unsigned char *data, unsigned char pixel_art);
int roti_bind_tex(roti_tex *tex, uint32_t slot);
int roti_gen_texmat(roti_tex *tex);

#endif
