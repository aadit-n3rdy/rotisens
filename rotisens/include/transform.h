#ifndef ROTI_TRANSFORM_H
#define ROTI_TRANSFORM_H

#include "matrix.h"
#include "vec.h"

typedef struct roti_transform {
	vec2 pos;
	float angle;
	mat3 mat;
} roti_transform;

int roti_transform_gen_mvp(struct roti_transform* transform);

#endif
