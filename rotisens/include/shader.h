#ifndef ROTI_SHADER_H
#define ROTI_SHADER_H

#include "color.h"
#include "vec.h"
#include "matrix.h"

#include <stdint.h>

typedef uint32_t roti_shader;

uint32_t roti_compile_shader(const char *fname, uint32_t shader_type);

roti_shader roti_shader_init(const char *vert_fname, const char *frag_fname);

void roti_shader_bind(roti_shader shader);

int roti_shader_uniform_vec3(roti_shader shader, const char* name, vec3 vec);

int roti_shader_uniform_color(roti_shader shader, const char *name, color col);

int roti_shader_uniform_ui(roti_shader shader, const char* name, uint32_t val);

int roti_shader_uniform_i(roti_shader shader, const char* name, int32_t val);

int roti_shader_uniform_f(roti_shader shader, const char* name, float val);

int roti_shader_uniform_mat3(roti_shader shader, const char *name, mat3 mat);

#endif
