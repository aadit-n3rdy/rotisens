#ifndef ROTI_COLOR_H
#define ROTI_COLOR_H

typedef struct color {
	float r;
	float g;
	float b;
	float a;
} color;

#endif
