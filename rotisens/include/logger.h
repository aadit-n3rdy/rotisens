#ifndef ROTISENS_LOGGER_H
#define ROTISENS_LOGGER_H

#include <stdbool.h>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glad/glad.h>


#define ROTI_LOG_LOG 0
#define ROTI_LOG_MSG 1
#define ROTI_LOG_WARN 2
#define ROTI_LOG_ERROR 3

int roti_log(const char *msg, int severity);

int roti_set_color_log(bool color_log);

int roti_set_gl_debug(bool enable);

#endif
