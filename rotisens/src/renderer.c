#include "renderer.h"

#include "logger.h"

#include <string.h>

static float default_buf[] = {
	-0.5, -0.5, 0.0, 0.0, 	// CENTER
	0.5, -0.5, 1.0, 0.0,
	0.5,  0.5, 1.0, 1.0,
	-0.5,  0.5, 0.0, 1.0,

	0.0,  0.0, 0.0, 0.0,	// TOPLEFT
	1.0,  0.0, 1.0, 0.0,
	1.0,  1.0, 1.0, 1.0,
	0.0,  1.0, 0.0, 1.0,

	-1.0,  0.0, 0.0, 0.0,	//TOPRIGHT
	0.0,  0.0, 1.0, 0.0,
	0.0,  1.0, 1.0, 1.0,
	-1.0,  1.0, 0.0, 1.0,

	0.0, -1.0, 0.0, 0.0,	//BOTTOMLEFT
	1.0, -1.0, 1.0, 0.0,
	1.0,  0.0, 1.0, 1.0,
	0.0,  0.0, 0.0, 1.0,

	-1.0, -1.0, 0.0, 0.0,	//BOTTOMRIGHT
	0.0, -1.0, 1.0, 0.0,
	0.0,  0.0, 1.0, 1.0,
	-1.0,  0.0, 0.0, 1.0
};



int roti_renderer_register_sprite(roti_renderer *renderer, roti_sprite *sprite) {
	int i = 0;
	for (i = 0; i < 512; i++) {
		if (renderer->sprites[i] == NULL) {
			renderer->sprites[i] = sprite;
			break;
		}
	}
	if (i == 512) {
		roti_log("Sprite limit reached", ROTI_LOG_ERROR);
		return -1;
	}
	return 0;
}

int roti_renderer_load_shaders(roti_renderer *renderer, char* vert_path, char* frag_path) {
	renderer->shader = roti_shader_init(vert_path, frag_path);
	if (renderer->shader == 0) {
		return -1;
	}
	return 0;
}

int roti_renderer_init(roti_renderer *renderer, char* vert_path, char* frag_path, roti_window *window, roti_transform *camera) {
	renderer->window = window;
	renderer->camera = camera;
	roti_renderer_load_shaders(renderer, vert_path, frag_path);
	glGenBuffers(1, &renderer->vbo);
	glGenVertexArrays(1, &renderer->vao);
	glBindVertexArray(renderer->vao);
	glBindBuffer(GL_ARRAY_BUFFER, renderer->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(default_buf), default_buf, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4*sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4*sizeof(float), (void*)(2*sizeof(float)));
	glEnableVertexAttribArray(1);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	memset(renderer->sprites, 0, sizeof(renderer->sprites));

	return 0;
}

int roti_render_sprites(roti_renderer *renderer) {
	unsigned int i = 0;
	roti_transform cam_rev;
	if (renderer->camera != NULL) {
		cam_rev.pos = (vec2) {-renderer->camera->pos.x, -renderer->camera->pos.y};
		cam_rev.angle = -renderer->camera->angle;
		roti_transform_gen_mvp(&cam_rev);
	}
	for (i = 0; i < 512; i++) {
		if (renderer->sprites[i] != NULL) {
			roti_sprite_gen_mat(renderer->sprites[i]);
			roti_shader_bind(renderer->shader);

			roti_transform_gen_mvp(renderer->sprites[i]->transform);
			roti_multip_mat3(renderer->sprites[i]->transform->mat, renderer->sprites[i]->transmat, renderer->mvp);
			if (renderer->camera != NULL) {
				roti_multip_mat3(cam_rev.mat, renderer->mvp, renderer->mvp);
			}
			roti_multip_mat3(renderer->window->mat, renderer->mvp, renderer->mvp);

			roti_shader_uniform_mat3(renderer->shader, "mvp", renderer->mvp);

			roti_shader_uniform_color(renderer->shader, "color_mask", renderer->sprites[i]->col);
			roti_shader_uniform_i(renderer->shader, "tex", 0);
			roti_shader_uniform_mat3(renderer->shader, "tex_mat", renderer->sprites[i]->texmat);
			roti_shader_uniform_f(renderer->shader, "z", renderer->sprites[i]->z);
			roti_bind_tex(renderer->sprites[i]->tex, 0);

			glBindVertexArray(renderer->vao);
			glBindBuffer(GL_ARRAY_BUFFER, renderer->vbo);
			glDrawArrays(GL_TRIANGLE_FAN, renderer->sprites[i]->alignment, 4);
		}
	}
	return 0;
}
