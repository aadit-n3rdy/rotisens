#include "window.h"
#include <stdio.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "logger.h"

static bool roti_gl_context = false;

static void glfw_error_callback(int error, const char *desc) {
	roti_log(desc, ROTI_LOG_ERROR);
}

static void glfw_resize_callback(GLFWwindow *window,int width,
		int height) {
	roti_window *win = glfwGetWindowUserPointer(window);
	win->width = width;
	win->height = height;
	roti_gen_winmat(win);
}

int roti_init() {
	if (!glfwInit()) {
		roti_log("Could not init glfw", ROTI_LOG_ERROR);
		return -1;
	}
	glfwSetErrorCallback(glfw_error_callback);
	return 0;
}

int roti_init_window(roti_window *win) {
	if (!glfwInit()) {
		roti_log("Could not init glfw", ROTI_LOG_ERROR);
		return -1;
	}
	glfwSetErrorCallback(glfw_error_callback);

	if (win->monitor == NULL) {
		win->monitor = glfwGetPrimaryMonitor();
	}

	if (win->width == 0 || win->height == 0) {
		glfwGetMonitorWorkarea(win->monitor, NULL, NULL, &win->width, &win->height);
	}
	if (win->mode == ROTI_WIN_FULLSCRN) {
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		win->ptr = glfwCreateWindow(win->width, win->height, win->title,
				win->monitor, NULL);
	} else if (win->mode == ROTI_WIN_WINDOWED_FULLSCRN) {
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		win->ptr = glfwCreateWindow(win->width, win->height, win->title,
				NULL, NULL);
	} else {
		if (win->resizeable) {
			glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
		} else {
			glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		}
		win->ptr = glfwCreateWindow(win->width, win->height, win->title,
				NULL, NULL);
	}
	if (win->ptr == NULL) {
		roti_log("Could not create window", ROTI_LOG_ERROR);
		return -1;
	}

	glfwMakeContextCurrent(win->ptr);
	glfwSetWindowUserPointer(win->ptr, win);
	glfwSetWindowSizeCallback(win->ptr, glfw_resize_callback);
	if (!roti_gl_context) {
		gladLoadGL();
		roti_gl_context = true;
	}
	if (win->vsync) {
		glfwSwapInterval(1);
	}
	roti_gen_winmat(win);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_DEPTH_TEST);
	return 0;
}

int roti_gen_winmat(roti_window *win) {
	mat3 tmp;
	roti_scale_mat3((vec2){ 2.0f/(float)win->width, -2.0f/(float)win->height },
			win->mat);
	roti_translate_mat3((vec2){.x=-1.0f, .y=1.0f}, tmp);
	roti_multip_mat3(tmp, win->mat, win->mat);
	char tmp_str[32];
	sprintf(tmp_str, "%d %d", win->width, win->height);
	roti_log(tmp_str, ROTI_LOG_LOG);
	return 0;

}

int roti_set_bg(color col) {
	glClearColor(col.r, col.g, col.b, col.a);
	return 0;
}

int roti_clear() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	return 0;
}

