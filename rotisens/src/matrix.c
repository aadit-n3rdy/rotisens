#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdio.h>

#include "matrix.h"
#include "vec.h"
#include "logger.h"

int roti_multip_mat3(mat3 m1, mat3 m2, mat3 result) {
	int r = 0;
	int c = 0;
	int i = 0;
	float sum = 0;
	int ret = 0;
	if (m1 == result || m2 == result) {
		mat3 tmp;
		ret = roti_multip_mat3(m1, m2, tmp);
		memcpy(result, tmp, sizeof(mat3));
		goto done;
	} else {
		for (r = 0; r < 3; r++) {
			for (c = 0; c < 3; c++) {
				sum = 0;
				for (i = 0; i < 3; i++) {
					sum += m1[r][i] * m2[i][c];
				}
				result[r][c] = sum;
			}
		}
	}
done:
	return ret;
	
}

vec3 roti_multip_mat3_vec3(mat3 m, vec3 v) {
	int r = 0;
	float result[3] = {0, 0, 0};
	for (r = 0; r < 3; r++) {
		result[r] = m[r][0] * v.x +
			m[r][1] * v.y +
			m[r][2] * v.z;
	}
	return (vec3){result[0], result[1], result[2]};
}

int roti_translate_mat3(vec2 v, mat3 dest) {
	dest[0][0] = 1;
	dest[0][1] = 0;
	dest[0][2] = v.x;
	dest[1][0] = 0;
	dest[1][1] = 1;
	dest[1][2] = v.y;
	dest[2][0] = 0;
	dest[2][1] = 0;
	dest[2][2] = 1;
	return 0;
}

int roti_scale_mat3(vec2 v, mat3 dest) {
	bzero((float*)dest, sizeof(mat3));
	dest[0][0] = v.x;
	dest[1][1] = v.y;
	dest[2][2] = 1;
	return 0;
}

int roti_rot_mat3(float theta, mat3 dest) {
	float sint = sinf(theta);
	float cost = cosf(theta);
	dest[0][0] = cost;
	dest[0][1] = -sint;
	dest[0][2] = 0;
	dest[1][0] = sint;
	dest[1][1] = cost;
	dest[1][2] = 0;
	dest[2][0] = 0;
	dest[2][1] = 0;
	dest[2][2] = 1;
	return 0;
}

int roti_print_mat3(mat3 m) {
	int i = 0;
	int j = 0;
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			printf("%10f ", m[i][j]);
		}
		printf("\n");
	}
	return 0;
}

