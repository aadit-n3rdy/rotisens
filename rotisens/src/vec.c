#include "vec.h"

#include <math.h>

vec3 roti_cross_vec3 (vec3 v1, vec3 v2) {
	vec3 ret;
	ret.x = v1.y * v2.z - v2.y * v1.z;
	ret.y = v1.z * v2.x - v1.x * v2.z;
	ret.z = v1.x * v2.y - v1.y * v2.x;
	return ret;
}

float roti_dot_vec3 (vec3 v1, vec3 v2) {
	return v1.x*v2.x + v1.y+v2.y + v1.z+v2.z;
}

vec3 roti_normalise_vec3 (vec3 v) {
	vec3 result;
	if (v.x == 0 && v.y == 0 && v.z == 0) {
		return v;
	}
	float mag = 1.0f/sqrtf(v.x*v.x +
			v.y * v.y +
			v.z * v.z);
	result.x = v.x * mag;
	result.y = v.y * mag;
	result.z = v.z * mag;
	return result;
}

vec3 roti_multip_vec3_float(vec3 v, float f) {
	vec3 result = {v.x * f,
		v.y * f,
		v.z * f};
	return result;
}

vec3 roti_add_vec3(vec3 v1, vec3 v2) {
	vec3 result = { v1.x + v2.x,
		v1.y + v2.y,
		v1.z + v2.z};
	return result;
}

vec2 roti_normalise_vec2(vec2 v) {
	float mag = 1.0f/sqrtf(v.x*v.x + v.y*v.y);
	return roti_multip_vec2_float(v, mag);
}

vec2 roti_multip_vec2_float(vec2 v, float f) {
	return (vec2){v.x * f, v.y * f};
}

vec2 roti_add_vec2(vec2 v1, vec2 v2) {
	return (vec2){v1.x + v2.x, v1.y + v2.y};
}

vec2 roti_rotate_vec2(vec2 v, float angle) {
	float theta = atan2f(v.y, v.x);
	float mag = sqrtf(v.x*v.x + v.y*v.y);
	theta += angle;
	v.x = mag * sinf(theta);
	v.y = mag * cosf(theta);
	return v;
}
