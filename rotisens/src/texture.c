#include "texture.h"

#include "stb_image.h"

#include <glad/glad.h>

#include <stdint.h>

#include "logger.h"

int roti_gen_tex(roti_tex *tex, const char* path, unsigned char pixel_art) {
	glGenTextures(1, &tex->id);
	unsigned char *data = stbi_load(path, 
			&tex->width, &tex->height, &tex->channels, tex->channels);
	glBindTexture(GL_TEXTURE_2D, tex->id);
	if (!data) {
		roti_log("Could not load texture", ROTI_LOG_ERROR);
		return -1;
	}

	roti_gen_tex_raw(tex, data, pixel_art);
	roti_gen_texmat(tex);
	stbi_image_free(data);
	return 0;
}

int roti_bind_tex(roti_tex *tex, uint32_t slot) {
	glActiveTexture(GL_TEXTURE0 + slot);
	glBindTexture(GL_TEXTURE_2D, tex->id);
	return 0;
}

int roti_gen_tex_raw(roti_tex *tex, const unsigned char *data, unsigned char pixel_art) {
	glGenTextures(1, &tex->id);
	glBindTexture(GL_TEXTURE_2D, tex->id);
	GLenum format = GL_RGB;
	GLenum internal_format = GL_RGB;
	if (tex->channels == 1) {
		format = GL_RED;
		internal_format = format;
	} else if (tex->channels == 2) {
		format = GL_RED;
		internal_format = format;
	} else if (tex->channels == 3) {
		format = GL_RGB;
		internal_format = format;
	} else if (tex->channels == 4) {
		format = GL_RGBA;
		internal_format = format;
	}
	glTexImage2D(GL_TEXTURE_2D, 0, internal_format, tex->width, tex->height, 
			0, format, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	if (pixel_art) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	} else {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	glGenerateMipmap(GL_TEXTURE_2D);
	return 0;
}

int roti_gen_texmat(roti_tex *tex) {
	roti_scale_mat3((vec2){1.0f/(float)tex->width, 1.0f/(float)tex->height}, tex->mat);
	return 0;
}
