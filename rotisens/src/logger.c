#include "logger.h"

#include <stdio.h>
#include <stdbool.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glad/glad.h>


static const char* ANSI_COLOR_RED    = "\x1b[31m";
static const char* ANSI_COLOR_GREEN  = "\x1b[32m";
static const char* ANSI_COLOR_YELLOW = "\x1b[33m";
static const char* ANSI_COLOR_RESET  = "\x1b[0m";

static int roti_color_log = true;

static void gl_message_callback ( GLenum source,
		GLenum type,
		GLuint id,
		GLenum severity,
		GLsizei length,
		const GLchar* message,
		const void* userParam )
{
	fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
			( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
			type, severity, message );
}

int roti_set_color_log(bool color_log) {
	roti_color_log = color_log;
	return 0;
}

int roti_log(const char *msg, int severity) {
	if (roti_color_log) {
		switch (severity) {
			case ROTI_LOG_LOG:
				printf("LOG:  ");
				break;
			case ROTI_LOG_MSG:
				printf("%sMSG: %s ", 
						ANSI_COLOR_GREEN, 
						ANSI_COLOR_RESET);
				break;
			case ROTI_LOG_WARN:
				printf("%sWARN:%s ", 
						ANSI_COLOR_YELLOW, 
						ANSI_COLOR_RESET);
				break;
			case ROTI_LOG_ERROR:
				printf("%sERR: %s ", 
						ANSI_COLOR_RED, 
						ANSI_COLOR_RESET);
		}
	} else {
		switch (severity) {
			case ROTI_LOG_LOG:
				printf("LOG:  ");
				break;
			case ROTI_LOG_MSG:
				printf("MSG:  ");
				break;
			case ROTI_LOG_WARN:
				printf("WARN: ");
				break;
			case ROTI_LOG_ERROR:
				printf("ERR:  ");
		}
	}
	printf("%s\n", msg);
	return 0;
}

int roti_set_gl_debug(bool enable) {
	if (enable) {
		glEnable(GL_DEBUG_OUTPUT);
		glDebugMessageCallback(gl_message_callback, 0);
	} else {
		glDisable(GL_DEBUG_OUTPUT);
	}
	return 0;
}
