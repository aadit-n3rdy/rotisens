#include "shader.h"
#include "color.h"

#include <glad/glad.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <strings.h>
#include <string.h>

#include "logger.h"

#define ROTI_SHADER_MAX_LEN 4096

uint32_t roti_compile_shader(const char *fname, uint32_t shader_type) {
	unsigned int ret = 0;
	char *buffer = malloc(ROTI_SHADER_MAX_LEN);
	int len;
	unsigned int shader_id;
	FILE *fp  = fopen(fname, "r");
	if (fp) {
		len = fread(buffer, sizeof(char), 4096, fp);
		buffer[len] = '\0';
	} else {
		roti_log("Could not open shader source", ROTI_LOG_ERROR);
		goto done;
	}
	shader_id = glCreateShader(shader_type);
	glShaderSource(shader_id, 1, (const GLchar *const *)&buffer, &len);
	glCompileShader(shader_id);
	int32_t status;
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);
	if (!status) {
		bzero(buffer, 2048);
		glGetShaderInfoLog(shader_id, 2048, NULL, buffer);
		roti_log("Could not compile shader from file", ROTI_LOG_ERROR);
		roti_log(fname, ROTI_LOG_ERROR);
		roti_log(buffer, ROTI_LOG_ERROR);
		goto done;
	}
	ret = shader_id;
done:
	free(buffer);
	return ret;
}

roti_shader roti_shader_init(const char *vert_fname, const char *frag_fname) {
	uint32_t vert, frag;
	roti_shader ret;
	vert = roti_compile_shader(vert_fname, GL_VERTEX_SHADER);
	frag = roti_compile_shader(frag_fname, GL_FRAGMENT_SHADER);
	if (vert == 0 || frag == 0) {
		return 0;
	}
	ret = glCreateProgram();
	glAttachShader(ret, vert);
	glAttachShader(ret, frag);
	glLinkProgram(ret);
	int32_t status;
	glGetProgramiv(ret, GL_LINK_STATUS, &status);
	if (!status) {
		roti_log("Could not link shader", ROTI_LOG_ERROR);
		ret = 0;
	}
	glDeleteShader(vert);
	glDeleteShader(frag);
	return ret;
}

void roti_shader_bind(roti_shader shader) {
	glUseProgram(shader);
}

int roti_shader_uniform_vec3(roti_shader shader, const char* name, vec3 vec) {
	roti_shader_bind(shader);
	glUniform3f(glGetUniformLocation(shader, name),
			vec.x,
			vec.y,
			vec.z);
	return 0;
}

int roti_shader_uniform_color(roti_shader shader, const char *name, color col) {
	roti_shader_bind(shader);
	glUniform4f(glGetUniformLocation(shader, name),
			col.r,
			col.g,
			col.b,
			col.a);
	return 0;
}

int roti_shader_uniform_ui(roti_shader shader, const char* name, uint32_t val) {
	roti_shader_bind(shader);
	glUniform1ui(glGetUniformLocation(shader, name),
			val);
	return 0;
}

int roti_shader_uniform_i(roti_shader shader, const char* name, int32_t val) {
	roti_shader_bind(shader);
	glUniform1i(glGetUniformLocation(shader, name),
			val);
	return 0;
}

int roti_shader_uniform_f(roti_shader shader, const char* name, float val) {
	roti_shader_bind(shader);
	glUniform1f(glGetUniformLocation(shader, name),
			val);
	return 0;
}

int roti_shader_uniform_mat3(roti_shader shader, const char *name, mat3 mat) {
	roti_shader_bind(shader);
	glUniformMatrix3fv(glGetUniformLocation(shader, name),
			1,
			GL_TRUE,
			(float*)mat);
	return 0;
}
