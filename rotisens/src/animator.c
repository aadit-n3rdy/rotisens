#include "animator.h"
#include "logger.h"

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glad/glad.h>

#include <string.h>

int roti_animated_init(roti_animated *animated, roti_sprite *sprite) {
	animated->sprite = sprite;
	animated->cur_frame = 0;
	animated->last_update = 0;
	return 0;
}

int roti_animated_set_animation(roti_animated *animated, roti_animation *animation) {
	animated->cur_animation = animation;
	animated->cur_frame = 0;
	animated->last_update = glfwGetTime();
	animated->sprite->tex_topleft = animation->start;
	return 0;
}

int roti_animator_init(roti_animator *animator) {
	memset(animator->animations, 0, sizeof(animator->animations));
	return 0;
}

int roti_register_animated(roti_animator *animator, roti_animated *animated) {
	unsigned int i = 0;
	for (i = 0; i < 512; i++) {
		if (animator->animations[i] == NULL) {
			animator->animations[i] = animated;
			break;
		}
	}
	if (i == 512) {
		roti_log("Animator full", ROTI_LOG_ERROR);
		return -1;
	}
	return 0;
}

int roti_animator_update(roti_animator *animator) {
	unsigned int i = 0;
	double t = glfwGetTime();
	roti_animated *cur_animated;
	for (i = 0; i < 512; i++) {
		if (animator->animations[i] != NULL) {
			cur_animated = animator->animations[i];
			if (t - cur_animated->last_update >= cur_animated->cur_animation->delta) {
				cur_animated->last_update = t;
				roti_sprite *cur_sprite = cur_animated->sprite;
				roti_animation *cur = cur_animated->cur_animation;
				if (cur_animated->cur_frame == cur->total_frames) {
					cur_sprite->tex_topleft = cur->start;
					cur_animated->cur_frame = 0;
				} else {
					cur_sprite->tex_topleft.x += cur->offset.x;
					if (cur_sprite->tex_topleft.x >= cur_sprite->tex->width) {
						cur_sprite->tex_topleft.x = 0.0f;
					}
					cur_sprite->tex_topleft.y += cur->offset.y;
					if (cur_sprite->tex_topleft.y >= cur_sprite->tex->height) {
						cur_sprite->tex_topleft.y = 0.0f;
					}
					cur_animated->cur_frame++;
				}
			}
		}
	}
	return 0;
}


