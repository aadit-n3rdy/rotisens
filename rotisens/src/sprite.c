#include "sprite.h"

int roti_sprite_gen_texmat(struct roti_sprite* sprite) {
	mat3 tmp;
	roti_scale_mat3(sprite->tex_size, sprite->texmat);
	roti_translate_mat3(sprite->tex_topleft, tmp);
	roti_multip_mat3(tmp, sprite->texmat, sprite->texmat);
	roti_multip_mat3(sprite->tex->mat, sprite->texmat, sprite->texmat);
	return 0;
}

int roti_sprite_gen_mvp(struct roti_sprite* sprite) {
	mat3 tmp;
	roti_scale_mat3(sprite->size, sprite->transmat);
	roti_translate_mat3(sprite->offset, tmp);
	roti_multip_mat3(tmp, sprite->transmat, sprite->transmat);
	return 0;
}

int roti_sprite_gen_mat(struct roti_sprite* sprite) {
	roti_sprite_gen_texmat(sprite);
	roti_sprite_gen_mvp(sprite);
	return 0;
}
