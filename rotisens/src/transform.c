#include "transform.h"

int roti_transform_gen_mvp(struct roti_transform* transform) {
	mat3 tmp;
	roti_rot_mat3(transform->angle, transform->mat);
	roti_translate_mat3(transform->pos, tmp);
	roti_multip_mat3(tmp, transform->mat, transform->mat);
	return 0;
}
